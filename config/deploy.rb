# Used by logging
set :application, 'ajax-capified'

# The directory on the remote system that will contain release versions
# and the "current" release (which is a symlink to the last good release).
# NOTE: This should be an absolute path (beginning with /).
set :deploy_to, '/home/ubuntu/test'

# If your code is checked into a repository for source control . . .
#   set :repository,  "set your repository location here"
#   set :scm, :subversion
# In this example, we're going to skip source control.

ssh_options[:forward_agent] = true
set :scm, :git
set :repository, "git@bitbucket.org:inomoz/inomoz-bp.git";
set :deploy_via, :remote_cache

set :normalize_asset_timestamps, false

# Log in as this user for setup and deployment
set :user, 'ubuntu'

# We want to avoid using the root user
set :use_sudo, false

# Our single server will serve all of the available roles; Capistrano
# is designed to allow for having the different roles be served by
# different computers.
server '54.247.82.13', :web, :app, :db, :primary => true

# Specify the private key you will use
#ssh_options[:keys] = ["aws_key.pem"]
ssh_options[:keys] = %w(~/.ssh/aws_key.pem)
